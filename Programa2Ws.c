//Generar una matriz de NxN y llenarla por el usuario solo con valores multiplos de 3 e impares
//de la matriz uno obtener una segunda matriz con el 5% de los valores de la matriz uno 
//mostrar ambas matrices y elborar:
//El numero menor de la matriz 1
//Mostrar los lugares en los que se numeros mayores a 10 en la matriz 2
#include <stdio.h>
#include <stdlib.h>
int main(){
int i,j,N,aux,error;
printf("Inserte el tamano de la matriz cuadrada:");
scanf("%d",&N);
int matriz[N][N];
float matriz2[N][N];
printf("\nLa Matriz debe ser llenada con valores que sean multiplos de 3 e impares");
for(j=0;j<N;j++){
    for(i=0;i<N;i++){
        printf("\nInserte el valor  %d , %d de la matriz: ",i+1,j+1);
        scanf("%d",&aux);
        if(aux%3==0){
            if(aux%2!=0){
                matriz[j][i]=aux;
                
            }
            else{
                printf("\nError 2: El numero insertado no es un impar, vuelva a intentarlo");
                i--;
                error=1;
            }
        }
        else{
            printf("\nError 1: El numero insertado no es un multiplo de 3, vuelva a intentarlo");
            i--;
            error=1;
            
        }
    }
    if(error==1){
        j--;
        error=0;
    }
}
for(j=0;j<N;j++){
    for(i=0;i<N;i++){
        matriz2[j][i]=matriz[j][i]*.05;
    }
}
//system("clear");
system("cls");
printf("Primera matriz\n");
for(j=0;j<N;j++){
    for(i=0;i<N;i++){
        printf("  %d  ",matriz[j][i]);
    }
    printf("\n");
}
printf("\nSegunda matriz\n");
for(j=0;j<N;j++){
    for(i=0;i<N;i++){
        printf("  %.2f  ",matriz2[j][i]);
    }
    printf("\n");
}
int menor;
menor=matriz[0][0];
for(j=0;j<N;j++){
    for(i=0;i<N;i++){
        if(menor>matriz[j][i]){
            menor=matriz[j][i];
        }
    }
}
printf("\nEl numero menor de la matriz 1 es: %d",menor);
for(j=0;j<N;j++){
    for(i=0;i<N;i++){
        if(matriz2[j][i]>10){
            printf("En la pocision %d , %d de la segunda matriz existe un valor mayor a diez",j+1,i+1);
        }
    }
}

return 0;
}