//Generar una matriz de M*6 con las notas de 6 materias con M alumno Elaborar:
// a) La nota promedio de cada estudiante 
// b) El numero de estudiantes que aprobaron cada materia
// c) El numero de estudiantes que reprobaron cada materia
// d) La nota promedio de cada materia
#include  <stdlib.h>
#include  <stdio.h>
int main(){
    int M,j,i,acumulador1;
    printf("Inserte el numero de alumnos a evaluar:");
    scanf("%d",&M);
    int calificaciones[M][6];//declaramos la matriz 
    float promedio[M];//vector promedio alumnos
    int aprobados[6];//vector aprobados por materia
    int reprobados[6];//vector reprobados materia
    float promediomaterias[6];//vector promedio materias
    for(j=0;j<M;j++){
        for(i=0;i<6;i++){
            printf("\n Inserte la nota de la materia %d del alumno %d :",i+1,j+1);// El +1 hace una correcion de valores entre la logica y la expocision de valores 
            scanf("%d",&calificaciones[j][i]);//llenamos la matriz de calificaciones
        }
    }
    acumulador1=0;
    for(j=0;j<M;j++){
      for(i=0;i<6;i++){
          acumulador1 = calificaciones[j][i] + acumulador1;
      }
      promedio[j]=acumulador1/6;//realizamos el promedio y lo guardamos en el vector  
      acumulador1=0;
    }
    promediomaterias[0]=0;
    promediomaterias[1]=0;
    promediomaterias[2]=0;
    promediomaterias[3]=0;
    promediomaterias[4]=0;
    promediomaterias[5]=0;
    for(j=0;j<M;j++){
        for(i=0;i<6;i++){
        if (calificaciones[j][i]<6){
            reprobados[i]++;
        }
        else {
            aprobados[i]++;
        }
        switch (i)
        {
        case 0:
            promediomaterias[i]+=calificaciones[j][i];
            break;
        case 1:
            promediomaterias[i]+=calificaciones[j][i];
            break;
        case 2:
            promediomaterias[i]+=calificaciones[j][i];
            break;
        case 3:
            promediomaterias[i]+=calificaciones[j][i];
            break;
        case 4:
            promediomaterias[i]+=calificaciones[j][i];
            break;
        case 5:
            promediomaterias[i]+=calificaciones[j][i];
            break;
        }
        }
   }
   //system("clear");
   system("cls");
   for(i=0;i<6;i++){
       promediomaterias[i]=promediomaterias[i]/M;
   }
   for(j=0;j<M;j++){
       printf("\nEl promedio del alumno %d es: %.2f",j+1,promedio[j]);
   }
   for(i=0;i<6;i++){
       printf("\nMateria %d :",i+1);
       printf("\nAprobados: %d",aprobados[i]);
       printf("\nReprobados: %d",reprobados[i]);
       printf("\nPromedio: %.2f",promediomaterias[i]);
   }
return 0;
}